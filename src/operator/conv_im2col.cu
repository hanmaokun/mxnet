/*!
 * Copyright (c) 2017 by Contributors
 * \file conv_im2col.cu
 * \brief
 * \author Bing Xu, Jun Wu
*/

#include "./conv_im2col-inl.h"
#include <vector>
#if MXNET_USE_CUDNN == 1
//#include "./cudnn_convolution-inl.h"
#endif  // MXNET_USE_CUDNN

namespace mxnet {
namespace op {

template<>
Operator* CreateOp<gpu>(Conv_im2colParam param, int dtype,
                        std::vector<TShape> *in_shape,
                        std::vector<TShape> *out_shape,
                        Context ctx) {
  Operator *op = NULL;
  // If 1D conv_im2col, use MXNet implementation
  if (param.kernel.ndim() == 1) {
    MSHADOW_REAL_TYPE_SWITCH(dtype, DType, {
      op = new Conv_im2colOp<gpu, DType>(param);
    })
    return op;
  }
#if MXNET_USE_CUDNN == 1
  if (param.dilate.Size() == 1 && !param.cudnn_off) {
    MSHADOW_REAL_TYPE_SWITCH(dtype, DType, {
      //op = new CuDNNConvolutionOp<DType>(param, *in_shape, *out_shape, ctx);
      op = new Conv_im2colOp<gpu, DType>(param);
    })
  } else {
    MSHADOW_REAL_TYPE_SWITCH(dtype, DType, {
      op = new Conv_im2colOp<gpu, DType>(param);
    })
  }
#else
  MSHADOW_REAL_TYPE_SWITCH(dtype, DType, {
    op = new Conv_im2colOp<gpu, DType>(param);
  })
#endif  // MXNET_USE_CUDNN
  return op;
}

}  // namespace op
}  // namespace mxnet

