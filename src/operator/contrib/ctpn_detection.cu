/*!
 * Copyright (c) 2016 by Contributors
 * \file ctpn_detection.cu
 * \brief CtpnDetection op
 * \author Joshua Zhang
*/
#include "./ctpn_detection-inl.h"
#include <mshadow/cuda/tensor_gpu-inl.cuh>

#define CTPN_DETECTION_CUDA_CHECK(condition) \
  /* Code block avoids redefinition of cudaError_t error */ \
  do { \
    cudaError_t error = condition; \
    CHECK_EQ(error, cudaSuccess) << " " << cudaGetErrorString(error); \
  } while (0)

namespace mshadow {
namespace cuda {
template<typename DType>
__device__ void Clip(DType *value, const DType lower, const DType upper) {
  if ((*value) < lower) *value = lower;
  if ((*value) > upper) *value = upper;
}

template<typename DType>
__device__ void CalculateOverlap(const DType *a, const DType *b, DType *iou) {
  DType w = max(DType(0), min(a[2], b[2]) - max(a[0], b[0]));
  DType h = max(DType(0), min(a[3], b[3]) - max(a[1], b[1]));
  DType i = w * h;
  DType u = (a[2] - a[0]) * (a[3] - a[1]) + (b[2] - b[0]) * (b[3] - b[1]) - i;
  (*iou) =  u <= 0.f ? static_cast<DType>(0) : static_cast<DType>(i / u);
}

template<typename DType>
__global__ void DetectionForwardKernel(DType *out, const DType *cls_prob,
                                       const DType *loc_pred, const DType *anchors,
                                       DType *temp_space, const int num_classes,
                                       const int num_anchors, const float threshold,
                                       const bool clip, const float vx,
                                       const float vy, const float vw,
                                       const float vh, const float nms_threshold,
                                       const bool force_suppress, const int nms_topk) {
  const int nbatch = blockIdx.x;  // each block for each batch
  int index = threadIdx.x;
  __shared__ int valid_count;
  out += nbatch * num_anchors * 6;
  cls_prob += nbatch * num_anchors * num_classes;
  loc_pred += nbatch * num_anchors * 2;

  if (index == 0) {
    valid_count = 0;
  }
  __syncthreads();

  /* debug with anchors. 
  for (int i = index; i < num_anchors; i += blockDim.x) {
    int anchor_ofs = i*4;
    int pos = i*6;
    float score = 0.0;
    if ((i >= 27450) && (i < 27460)){
      score = 1.0;
    }
    out[pos + 0] = 0;
    out[pos + 1] = score;
    out[pos + 2] = anchors[anchor_ofs + 0];
    out[pos + 3] = anchors[anchor_ofs + 1];
    out[pos + 4] = anchors[anchor_ofs + 2];
    out[pos + 5] = anchors[anchor_ofs + 3];
  }
  return;
  */

  // apply prediction to anchors
  for (int i = index; i < num_anchors; i += blockDim.x) {
    DType score = -1;
    int id = 0;
    for (int j = 1; j < num_classes; ++j) {
      DType temp = cls_prob[j * num_anchors + i];
      if (temp > score) {
        score = temp;
        id = j;
      }
    }
    if (id > 0 && score < threshold) {
      id = 0;
    }

    if (id > 0) {
      // valid class
      int pos = atomicAdd(&valid_count, 1);
      out[pos * 6] = id - 1;  // restore original class id
      out[pos * 6 + 1] = (id == 0 ? DType(-1) : score);
      int offset_anchor = i * 4;
      int offset_loc = i * 2;
      DType al = anchors[offset_anchor];
      DType at = anchors[offset_anchor + 1];
      DType ar = anchors[offset_anchor + 2];
      DType ab = anchors[offset_anchor + 3];
      DType aw = ar - al;
      DType ah = ab - at;
      DType ax = (al + ar) / 2.f;
      DType ay = (at + ab) / 2.f;
      /*
      if (loc_pred[offset_loc] == DType(-1) && loc_pred[offset_loc+1] == DType(-1)) {
        out[pos * 6 + 1] = DType(-1);
      }
      */
      //DType ox = loc_pred[offset_loc] * vx * aw + ax;
      DType oy = loc_pred[offset_loc] * vy * ah + ay;
      //DType ow = exp(loc_pred[offset_loc + 2] * vw) * aw / 2;
      DType oh = exp(loc_pred[offset_loc + 1] * vh) * ah / 2;
      DType xmin = ax - aw/2;
      DType ymin = oy - oh;
      DType xmax = ax + aw/2;
      DType ymax = oy + oh;
      if (clip) {
        Clip(&xmin, DType(0), DType(1));
        Clip(&ymin, DType(0), DType(1));
        Clip(&xmax, DType(0), DType(1));
        Clip(&ymax, DType(0), DType(1));
      }
      out[pos * 6 + 2] = xmin;
      out[pos * 6 + 3] = ymin;
      out[pos * 6 + 4] = xmax;
      out[pos * 6 + 5] = ymax;
    }
  }
  __syncthreads();

  if (valid_count < 1 || nms_threshold <= 0 || nms_threshold > 1) return;
  // if (index == 0) printf("%d\n", valid_count);

  // descent sort according to scores
  const int size = valid_count;
  temp_space += nbatch * num_anchors * 6;
  DType *src = out;
  DType *dst = temp_space;
  for (int width = 2; width < (size << 1); width <<= 1) {
    int slices = (size - 1) / (blockDim.x * width) + 1;
    int start = width * index * slices;
    for (int slice = 0; slice < slices; ++slice) {
      if (start >= size) break;
      int middle = start + (width >> 1);
      if (middle > size) middle = size;
      int end = start + width;
      if (end > size) end = size;
      int i = start;
      int j = middle;
      for (int k = start; k < end; ++k) {
        DType score_i = i < size ? src[i * 6 + 1] : DType(-1);
        DType score_j = j < size ? src[j * 6 + 1] : DType(-1);
        if (i < middle && (j >= end || score_i > score_j)) {
          for (int n = 0; n < 6; ++n) {
            dst[k * 6 + n] = src[i * 6 + n];
          }
          ++i;
        } else {
          for (int n = 0; n < 6; ++n) {
            dst[k * 6 + n] = src[j * 6 + n];
          }
          ++j;
        }
      }
      start += width;
    }
    __syncthreads();
    src = src == out? temp_space : out;
    dst = dst == out? temp_space : out;
  }
  __syncthreads();

  if (src == temp_space) {
    // copy from temp to out
    for (int i = index; i < size * 6; i += blockDim.x) {
      out[i] = temp_space[i];
    }
    __syncthreads();
  }

  // keep top k detections
  int ntop = size;
  if (nms_topk > 0 && nms_topk < ntop) {
    ntop = nms_topk;
    for (int i = ntop + index; i < size; i += blockDim.x) {
      out[i * 6] = -1;
    }
    __syncthreads();
  }

  // apply NMS
  for (int compare_pos = 0; compare_pos < ntop; ++compare_pos) {
    DType compare_id = out[compare_pos * 6];
    if (compare_id < 0) continue;  // not a valid positive detection, skip
    DType *compare_loc_ptr = out + compare_pos * 6 + 2;
    for (int i = compare_pos + index + 1; i < ntop; i += blockDim.x) {
      DType class_id = out[i * 6];
      if (class_id < 0) continue;
      if (force_suppress || (class_id == compare_id)) {
        DType iou;
        CalculateOverlap(compare_loc_ptr, out + i * 6 + 2, &iou);
        if (iou >= nms_threshold) {
          out[i * 6] = -1;
        }
      }
    }
    __syncthreads();
  }
}
}  // namespace cuda

template<typename DType>
inline void CtpnDetectionForward(const Tensor<gpu, 3, DType> &out,
                                     const Tensor<gpu, 3, DType> &cls_prob,
                                     const Tensor<gpu, 2, DType> &loc_pred,
                                     const Tensor<gpu, 2, DType> &anchors,
                                     const Tensor<gpu, 3, DType> &temp_space,
                                     const float threshold,
                                     const bool clip,
                                     const nnvm::Tuple<float> &variances,
                                     const float nms_threshold,
                                     const bool force_suppress,
                                     const int nms_topk) {
  CHECK_EQ(variances.ndim(), 4) << "Variance size must be 4";
  const int num_classes = cls_prob.size(1);
  const int num_anchors = cls_prob.size(2);
  const int num_batches = cls_prob.size(0);
  const int num_threads = cuda::kMaxThreadsPerBlock;
  int num_blocks = num_batches;
  //LOG(INFO) << "num_classes: " << num_classes;
  //LOG(INFO) << "num_anchors: " << num_anchors;
  //LOG(INFO) << "num_batches: " << num_batches;
  //LOG(INFO) << "num_threads: " << num_threads;
  cuda::CheckLaunchParam(num_blocks, num_threads, "CtpnDetection Forward");
  cudaStream_t stream = Stream<gpu>::GetStream(out.stream_);

  /* write data to file, for debug 
  const DType *p_cls_prob = cls_prob.dptr_;
  FILE *f_cls = fopen("/home/nlp/tmp/pcls_gpu.txt", "wt");
  for (int i=0; i<num_anchors; i++){
    for (int j = 1; j < num_classes; ++j) {
      DType temp = p_cls_prob[j * num_anchors + i];
      DType *h_data = (DType *)malloc(sizeof(temp));
      cudaMemcpy(h_data, p_cls_prob + j * num_anchors + i, sizeof(temp), cudaMemcpyDeviceToHost);
      fprintf(f_cls, "%lf ", *h_data);
    }
  }
  const DType *p_loc_pred = loc_pred.dptr_;
  FILE *f_loc = fopen("/home/nlp/tmp/ploc_gpu.txt", "wt");
  for (int i=0; i<num_anchors; i++){
      DType temp0 = p_loc_pred[i*2];
      DType temp1 = p_loc_pred[i*2+1];
      DType *h_data0 = (DType *)malloc(sizeof(temp0));
      DType *h_data1 = (DType *)malloc(sizeof(temp1));
      cudaMemcpy(h_data0, p_loc_pred + 2*i, sizeof(temp0), cudaMemcpyDeviceToHost);
      cudaMemcpy(h_data1, p_loc_pred + 2*i+1, sizeof(temp1), cudaMemcpyDeviceToHost);
      fprintf(f_loc, "%lf %lf, ", *h_data0, *h_data1);
  }
  const DType *p_anchor = anchors.dptr_;
  FILE *f_anc = fopen("/home/nlp/tmp/panchor_gpu.txt", "wt");
  for (int i=0; i<num_anchors; i++) {
      DType temp0 = p_anchor[i*4];
      DType *h_data0 = (DType *)malloc(sizeof(temp0));
      cudaMemcpy(h_data0, p_anchor + 4*i, sizeof(temp0), cudaMemcpyDeviceToHost);
      DType temp1 = p_anchor[i*4+1];
      DType *h_data1 = (DType *)malloc(sizeof(temp1));
      cudaMemcpy(h_data1, p_anchor + 4*i + 1, sizeof(temp1), cudaMemcpyDeviceToHost);
      DType temp2 = p_anchor[i*4+2];
      DType *h_data2 = (DType *)malloc(sizeof(temp2));
      cudaMemcpy(h_data2, p_anchor + 4*i + 2, sizeof(temp2), cudaMemcpyDeviceToHost);
      DType temp3 = p_anchor[i*4+3];
      DType *h_data3 = (DType *)malloc(sizeof(temp3));
      cudaMemcpy(h_data3, p_anchor + 4*i + 3, sizeof(temp3), cudaMemcpyDeviceToHost);
      fprintf(f_anc, "%lf, %lf %lf %lf ", *h_data0, *h_data1, *h_data2, *h_data3);
  }
  */
  cuda::DetectionForwardKernel<<<num_blocks, num_threads, 0, stream>>>(out.dptr_,
    cls_prob.dptr_, loc_pred.dptr_, anchors.dptr_, temp_space.dptr_,
    num_classes, num_anchors, threshold, clip,
    variances[0], variances[1], variances[2], variances[3],
    nms_threshold, force_suppress, nms_topk);
  /*
  const DType *p_out = out.dptr_;
  FILE *f_out = fopen("/home/nlp/tmp/pout_gpu.txt", "wt");
  for (int i=0; i<2220; i++) {
      DType temp0 = p_out[i*6];
      DType *h_data0 = (DType *)malloc(sizeof(temp0));
      cudaMemcpy(h_data0, p_out + 6*i, sizeof(temp0), cudaMemcpyDeviceToHost);
      DType temp1 = p_out[i*6];
      DType *h_data1 = (DType *)malloc(sizeof(temp1));
      cudaMemcpy(h_data1, p_out + 6*i + 1, sizeof(temp1), cudaMemcpyDeviceToHost);
      DType temp2 = p_out[i*6];
      DType *h_data2 = (DType *)malloc(sizeof(temp2));
      cudaMemcpy(h_data2, p_out + 6*i + 2, sizeof(temp2), cudaMemcpyDeviceToHost);
      DType temp3 = p_out[i*6];
      DType *h_data3 = (DType *)malloc(sizeof(temp3));
      cudaMemcpy(h_data3, p_out + 6*i + 3, sizeof(temp3), cudaMemcpyDeviceToHost);
      DType temp4 = p_out[i*6];
      DType *h_data4 = (DType *)malloc(sizeof(temp4));
      cudaMemcpy(h_data4, p_out + 6*i + 4, sizeof(temp4), cudaMemcpyDeviceToHost);
      DType temp5 = p_out[i*6];
      DType *h_data5 = (DType *)malloc(sizeof(temp5));
      cudaMemcpy(h_data5, p_out + 6*i + 5, sizeof(temp5), cudaMemcpyDeviceToHost);
      fprintf(f_out, "%lf %lf %lf %lf %lf %lf\n", *h_data0, *h_data1, *h_data2, *h_data3, *h_data4, *h_data5);
  }
  */
  CTPN_DETECTION_CUDA_CHECK(cudaPeekAtLastError());
}
}  // namespace mshadow

namespace mxnet {
namespace op {
template<>
Operator *CreateOp<gpu>(CtpnDetectionParam param, int dtype) {
  Operator *op = NULL;
  MSHADOW_REAL_TYPE_SWITCH(dtype, DType, {
    op = new CtpnDetectionOp<gpu, DType>(param);
  });
  return op;
}
}  // namespace op
}  // namespace mxnet
