/*!
 * Copyright (c) 2016 by Contributors
 * \file ctpn_prior.cu
 * \brief generate ctpn prior boxes cuda kernels
 * \author Joshua Zhang
*/

#include "./ctpn_prior-inl.h"
#include <mshadow/cuda/tensor_gpu-inl.cuh>

#define CTPNPRIOR_CUDA_CHECK(condition) \
  /* Code block avoids redefinition of cudaError_t error */ \
  do { \
    cudaError_t error = condition; \
    CHECK_EQ(error, cudaSuccess) << " " << cudaGetErrorString(error); \
  } while (0)

namespace mshadow {
namespace cuda {
template<typename DType>
__global__ void AssignPriors(DType *out, const float size,
                             const float sqrt_ratio, const int in_width,
                             const int in_height, const float step_x,
                             const float step_y, const float center_offy,
                             const float center_offx, const int stride,
                             const int offset) {
  int index = blockIdx.x * blockDim.x + threadIdx.x;
  if (index >= in_width * in_height) return;
  int r = index / in_width;
  int c = index % in_width;
  float center_x = (c) * step_x;
  float center_y = (r) * step_y;

  float w = step_x / 2;  // half width
  float h = (step_y * sqrt_ratio) / 2;  // half height
  DType *ptr = out + index * stride + 4 * offset;
  *(ptr++) = center_x - w;  // xmin
  *(ptr++) = center_y - h;  // ymin
  *(ptr++) = center_x + w;  // xmax
  *(ptr++) = center_y + h;  // ymax
  //printf("anchor: %f %f %f %f", center_x, w, center_y, h);
}
}  // namespace cuda

template<typename DType>
inline void CtpnPriorForward(const Tensor<gpu, 2, DType> &out,
                            const std::vector<float> &sizes,
                            const std::vector<float> &ratios,
                            const int in_width, const int in_height,
                            const std::vector<float> &steps,
                            const std::vector<float> &offsets) {
  CHECK_EQ(out.CheckContiguous(), true);
  cudaStream_t stream = Stream<gpu>::GetStream(out.stream_);
  DType *out_ptr = out.dptr_;
  const float step_x = steps[1];
  const float step_y = steps[0];
  const float offset_x = offsets[1];
  const float offset_y = offsets[0];
  const int num_sizes = static_cast<int>(sizes.size());
  const int num_ratios = static_cast<int>(ratios.size());

  const int num_thread = cuda::kMaxThreadsPerBlock;
  dim3 dimBlock(num_thread);
  dim3 dimGrid((in_width * in_height - 1) / num_thread + 1);
  cuda::CheckLaunchParam(dimGrid, dimBlock, "CtpnPrior Forward");

  const int stride = 4 * (num_sizes + num_ratios - 1);
  int offset = 0;
  /*
  LOG(INFO) << "IN CtpnPriorForward, GPU MODE.";
  LOG(INFO) << "in_width: " << in_width;
  LOG(INFO) << "in_height: " << in_height;
  LOG(INFO) << "sizes: " << sizes[0];
  LOG(INFO) << "num_ratios: " << num_ratios;
  LOG(INFO) << "num_sizes: " << num_sizes;
  LOG(INFO) << "ratios: " << ratios[0] << " " << ratios[1] << " " << ratios[2];
  LOG(INFO) << "steps: " << steps[0] << " " << steps[1];
  LOG(INFO) << "offsets: " << offsets[0] << " " << offsets[1];
  LOG(INFO) << "stride: " << stride;
  LOG(INFO) << "num_thread: " << num_thread;
  */
  
  // size = sizes[0], various ratios
  for (int j = 0; j < num_ratios; ++j) {
    cuda::AssignPriors<DType><<<dimGrid, dimBlock, 0, stream>>>(out_ptr,
      sizes[0], ratios[j], in_width, in_height, step_x, step_y,
       offset_y, offset_x, stride, offset);
    ++offset;
  }
  CTPNPRIOR_CUDA_CHECK(cudaPeekAtLastError());
  
  /*
  int count = 0;
  for (float r = 0; r < 1.0; r += step_y) {
    float center_y = r;
    for (float c = 0; c < 1.0; c += step_x) {
      float center_x = c;
      LOG(INFO) << "count: " << count;
      DType *ptr = out_ptr + count * stride;
      for (int j = 0; j < num_ratios; ++j) {
        float ratio = ratios[j];
        float w = step_x / 2.;
        float h = (step_y * ratio) / 2.;
        LOG(INFO) << "j: " << j;
        *(ptr++) = (center_x - w);  // xmin
        *(ptr++) = (center_y - h);  // ymin
        *(ptr++) = (center_x + w);  // xmax
        *(ptr++) = (center_y + h);  // ymax
      }
      ++count;
    }
  }
  */
}
}  // namespace mshadow

namespace mxnet {
namespace op {
template<>
Operator* CreateOp<gpu>(CtpnPriorParam param, int dtype) {
  Operator *op = NULL;
  MSHADOW_REAL_TYPE_SWITCH(dtype, DType, {
    op = new CtpnPriorOp<gpu, DType>(param);
  });
  return op;
}

}  // namespace op
}  // namespace mxnet
