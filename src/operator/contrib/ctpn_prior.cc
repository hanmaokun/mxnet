/*!
 * Copyright (c) 2016 by Contributors
 * \file ctpn_prior.cc
 * \brief generate ctpn prior boxes cpu implementation
 * \author Joshua Zhang
*/

#include "./ctpn_prior-inl.h"

namespace mshadow {
template<typename DType>
inline void CtpnPriorForward(const Tensor<cpu, 2, DType> &out,
                            const std::vector<float> &sizes,
                            const std::vector<float> &ratios,
                            const int in_width, const int in_height,
                            const std::vector<float> &steps,
                            const std::vector<float> &offsets) {
  const int num_sizes = static_cast<int>(sizes.size());
  const int num_ratios = static_cast<int>(ratios.size());
  int count = 0;

  /*
  LOG(INFO) << "IN CtpnPriorForward, CPU MODE.";
  LOG(INFO) << "in_width: " << in_width;
  LOG(INFO) << "in_height: " << in_height;
  LOG(INFO) << "sizes: " << sizes[0];
  LOG(INFO) << "num_ratios: " << num_ratios;
  LOG(INFO) << "ratios: " << ratios[0] << " " << ratios[1] << " " << ratios[2];
  LOG(INFO) << "steps: " << steps[0] << " " << steps[1];
  LOG(INFO) << "offsets: " << offsets[0] << " " << offsets[1];
  */
  
  float step_y = steps[0];
  float step_x = steps[1];

  for (float r = 0; r < 1.0; r += step_y) {
    float center_y = r;
    for (float c = 0; c < 1.0; c += step_x) {
      float center_x = c;
      for (int j = 0; j < num_ratios; ++j) {
        float ratio = ratios[j];
        float w = step_x / 2.;
        float h = (step_y * ratio) / 2.;
        out[count][0] = (center_x - w);  // xmin
        out[count][1] = (center_y - h);  // ymin
        out[count][2] = (center_x + w);  // xmax
        out[count][3] = (center_y + h);  // ymax
        ++count;
      }
    }
  }
  int rand_idx = rand() % (in_width*in_height*num_ratios);
  LOG(INFO) << "random anchor: " << "index: " << rand_idx << " # " << out[rand_idx][0] << " " << out[rand_idx][1] << " " << out[rand_idx][2] << " " << out[rand_idx][3];

}
}  // namespace mshadow

namespace mxnet {
namespace op {
template<>
Operator* CreateOp<cpu>(CtpnPriorParam param, int dtype) {
  Operator *op = NULL;
  MSHADOW_REAL_TYPE_SWITCH(dtype, DType, {
    op = new CtpnPriorOp<cpu, DType>(param);
  });
  return op;
}

Operator* CtpnPriorProp::CreateOperatorEx(Context ctx, std::vector<TShape> *in_shape,
                                       std::vector<int> *in_type) const {
  std::vector<TShape> out_shape, aux_shape;
  std::vector<int> out_type, aux_type;
  CHECK(InferShape(in_shape, &out_shape, &aux_shape));
  CHECK(InferType(in_type, &out_type, &aux_type));
  DO_BIND_DISPATCH(CreateOp, param_, in_type->at(0));
}

DMLC_REGISTER_PARAMETER(CtpnPriorParam);

MXNET_REGISTER_OP_PROPERTY(_contrib_CtpnPrior, CtpnPriorProp)
.add_argument("data", "Symbol", "Input data.")
.add_arguments(CtpnPriorParam::__FIELDS__())
.describe("Generate prior(anchor) boxes from data, sizes and ratios.");

}  // namespace op
}  // namespace mxnet
