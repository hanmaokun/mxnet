/*!
 * Copyright (c) 2016 by Contributors
 * \file ctpn_detection.cc
 * \brief CtpnDetection op
 * \author Joshua Zhang
*/
#include "./ctpn_detection-inl.h"
#include <algorithm>

namespace mshadow {
template<typename DType>
struct SortElemDescend {
  DType value;
  int index;

  SortElemDescend(DType v, int i) {
    value = v;
    index = i;
  }

  bool operator<(const SortElemDescend &other) const {
    return value > other.value;
  }
};

template<typename DType>
inline void TransformLocations(DType *out, const DType *anchors,
                               const DType *loc_pred, const bool clip,
                               const float vx, const float vy,
                               const float vw, const float vh) {
  // transform predictions to detection results
  DType al = anchors[0];
  DType at = anchors[1];
  DType ar = anchors[2];
  DType ab = anchors[3];

  DType aw = ar - al;
  DType ah = ab - at;
  DType ax = (al + ar) / 2.f;
  DType ay = (at + ab) / 2.f;

  //DType px = loc_pred[0];
  DType py = loc_pred[0];
  //DType pw = loc_pred[2];
  DType ph = loc_pred[1];
  
  if (py == DType(-1) && ph == DType(-1)) {
    out[1] = DType(-1);
  }
  
  //DType ox = px * vx * aw + ax;
  DType oy = py * vy * ah + ay;
  //DType ow = exp(pw * vw) * aw / 2;
  DType oh = exp(ph * vh) * ah / 2;

  DType ox = ax;
  DType ow = aw / 2;

  out[2] = clip ? std::max(DType(0), std::min(DType(1), ox - ow)) : (ox - ow);
  out[3] = clip ? std::max(DType(0), std::min(DType(1), oy - oh)) : (oy - oh);
  out[4] = clip ? std::max(DType(0), std::min(DType(1), ox + ow)) : (ox + ow);
  out[5] = clip ? std::max(DType(0), std::min(DType(1), oy + oh)) : (oy + oh);

  /*
  LOG(INFO) << "al: " << al << " at: " << at << " ar: " << ar << " ab: " << ab << "\n";
  LOG(INFO) << "aw: " << aw << " ah: " << ah << " ax: " << ah << " ay: " << ay << "\n";
  LOG(INFO) << "py: " << py << " ph: " << ph << "\n";
  LOG(INFO) << "oy: " << oy << " oh: " << oh << "\n";
  LOG(INFO) << "ox: " << ox << " ow: " << ow << "\n";
  LOG(INFO) << "out: " << out[0] << " " << out[1] << " "  << out[2] << " "  << out[3] << "\n";
  */
}

template<typename DType>
inline DType CalculateOverlap(const DType *a, const DType *b) {
  DType w = std::max(DType(0), std::min(a[2], b[2]) - std::max(a[0], b[0]));
  DType h = std::max(DType(0), std::min(a[3], b[3]) - std::max(a[1], b[1]));
  DType i = w * h;
  DType u = (a[2] - a[0]) * (a[3] - a[1]) + (b[2] - b[0]) * (b[3] - b[1]) - i;
  return u <= 0.f ? static_cast<DType>(0) : static_cast<DType>(i / u);
}

template<typename DType>
inline void CtpnDetectionForward(const Tensor<cpu, 3, DType> &out,
                                     const Tensor<cpu, 3, DType> &cls_prob,
                                     const Tensor<cpu, 2, DType> &loc_pred,
                                     const Tensor<cpu, 2, DType> &anchors,
                                     const Tensor<cpu, 3, DType> &temp_space,
                                     const float threshold,
                                     const bool clip,
                                     const nnvm::Tuple<float> &variances,
                                     const float nms_threshold,
                                     const bool force_suppress,
                                     const int nms_topk) {
  CHECK_EQ(variances.ndim(), 4) << "Variance size must be 4";
  const int num_classes = cls_prob.size(1);
  const int num_anchors = cls_prob.size(2);
  const int num_batches = cls_prob.size(0);
  const DType *p_anchor = anchors.dptr_;
  for (int nbatch = 0; nbatch < num_batches; ++nbatch) {
    const DType *p_cls_prob = cls_prob.dptr_ + nbatch * num_classes * num_anchors;
    const DType *p_loc_pred = loc_pred.dptr_ + nbatch * num_anchors * 2;
    DType *p_out = out.dptr_ + nbatch * num_anchors * 6;
    int valid_count = 0;

    LOG(INFO) << "num_classes: " << num_classes;

    // debug with anchors. 
    /*
    for (int i=0; i< num_anchors; i++) {
      const DType *anchor_ptr = p_anchor + i*4;
      DType *out_ptr = p_out + i*6;
      float score = 0.;
      if ((i >= 27450) && (i < 27460)){
      //if ( (i == 5626) || (i == 8766) || (i == 51986) || (i == 51996) || (i == 52006) || (i == 52016) || (i == 52026) || (i == 52036) || (i == 52046)) {
        score = 1.0;
      }
      out_ptr[0] = 0;
      out_ptr[1] = score;
      out_ptr[2] = anchor_ptr[0];
      out_ptr[3] = anchor_ptr[1];
      out_ptr[4] = anchor_ptr[2];
      out_ptr[5] = anchor_ptr[3];
    }
    return;
    */

    /* write data to file, for debug 
    FILE *f_cls = fopen("/home/nlp/tmp/pcls_cpu.txt", "wt");
    for (int i=0; i<num_anchors; i++){
      for (int j = 1; j < num_classes; ++j) {
        DType temp = p_cls_prob[j * num_anchors + i];
        fprintf(f_cls, "%lf ", temp);
      }
    }
    FILE *f_loc = fopen("/home/nlp/tmp/ploc_cpu.txt", "wt");
    for (int i=0; i<num_anchors; i++){
        DType temp0 = p_loc_pred[i*2];
        DType temp1 = p_loc_pred[i*2+1];
        fprintf(f_loc, "%lf %lf, ", temp0, temp1);
    }
    FILE *f_anc = fopen("/home/nlp/tmp/panchor_cpu.txt", "wt");
    for (int i=0; i<num_anchors; i++) {
      const DType *anchor_ptr = p_anchor + i*4;
      fprintf(f_anc, "%lf, %lf %lf %lf ", anchor_ptr[0], anchor_ptr[1], anchor_ptr[2], anchor_ptr[3]);
    }
    */
    for (int i = 0; i < num_anchors; ++i) {
      // find the predicted class id and probability
      DType score = -1;
      int id = 0;
      for (int j = 1; j < num_classes; ++j) {
        DType temp = p_cls_prob[j * num_anchors + i];
        if (temp > score) {
          score = temp;
          id = j;
        }
      }
      if (id > 0 && score < threshold) {
        id = 0;
      }
      if (id > 0) {
        // [id, prob, xmin, ymin, xmax, ymax]
        p_out[valid_count * 6] = id - 1;  // remove background, restore original id
        p_out[valid_count * 6 + 1] = (id == 0 ? DType(-1) : score);
        int offset_anchor = i * 4;
        int offset_loc_p = i * 2;
        TransformLocations(p_out + valid_count * 6, p_anchor + offset_anchor,
          p_loc_pred + offset_loc_p, clip, variances[0], variances[1],
          variances[2], variances[3]);
        ++valid_count;
      }
    }  // end iter num_anchors

    if (valid_count < 1 || nms_threshold <= 0 || nms_threshold > 1) continue;

    // sort and apply NMS
    Copy(temp_space[nbatch], out[nbatch], out.stream_);
    // sort confidence in descend order
    std::vector<SortElemDescend<DType>> sorter;
    sorter.reserve(valid_count);
    for (int i = 0; i < valid_count; ++i) {
      sorter.push_back(SortElemDescend<DType>(p_out[i * 6 + 1], i));
    }
    std::stable_sort(sorter.begin(), sorter.end());
    // re-order output
    DType *ptemp = temp_space.dptr_ + nbatch * num_anchors * 6;
    int nkeep = static_cast<int>(sorter.size());
    if (nms_topk > 0 && nms_topk < nkeep) {
      nkeep = nms_topk;
    }
    for (int i = 0; i < nkeep; ++i) {
      for (int j = 0; j < 6; ++j) {
        p_out[i * 6 + j] = ptemp[sorter[i].index * 6 + j];
      }
    }
    // apply nms
    for (int i = 0; i < valid_count; ++i) {
      int offset_i = i * 6;
      if (p_out[offset_i] < 0) continue;  // skip eliminated
      for (int j = i + 1; j < valid_count; ++j) {
        int offset_j = j * 6;
        if (p_out[offset_j] < 0) continue;  // skip eliminated
        if (force_suppress || (p_out[offset_i] == p_out[offset_j])) {
          // when foce_suppress == true or class_id equals
          DType iou = CalculateOverlap(p_out + offset_i + 2, p_out + offset_j + 2);
          if (iou >= nms_threshold) {
            p_out[offset_j] = -1;
          }
        }
      }
    }
    /*
    FILE *f_out = fopen("/home/nlp/tmp/pout_cpu.txt", "wt");
    for (int i=0; i<valid_count; i++) {
      fprintf(f_out, "%lf %lf %lf %lf %lf %lf\n", p_out[i*6], p_out[i*6+1], p_out[i*6+2], p_out[i*6+3], p_out[i*6+4], p_out[i*6+5]);
    }
    fprintf(f_out, " %d ", valid_count);
    */
  }  // end iter batch
}
}  // namespace mshadow

namespace mxnet {
namespace op {
template<>
Operator *CreateOp<cpu>(CtpnDetectionParam param, int dtype) {
  Operator *op = NULL;
  MSHADOW_REAL_TYPE_SWITCH(dtype, DType, {
    op = new CtpnDetectionOp<cpu, DType>(param);
  });
  return op;
}

Operator* CtpnDetectionProp::CreateOperatorEx(Context ctx,
                                                  std::vector<TShape> *in_shape,
                                                  std::vector<int> *in_type) const {
  std::vector<TShape> out_shape, aux_shape;
  std::vector<int> out_type, aux_type;
  CHECK(InferShape(in_shape, &out_shape, &aux_shape));
  CHECK(InferType(in_type, &out_type, &aux_type));
  DO_BIND_DISPATCH(CreateOp, param_, in_type->at(0));
  //CreateOp<cpu>(param_, in_type->at(0));
}

DMLC_REGISTER_PARAMETER(CtpnDetectionParam);
MXNET_REGISTER_OP_PROPERTY(_contrib_CtpnDetection, CtpnDetectionProp)
.describe("Convert ctpn detection predictions.")
.add_argument("rpn_cls_preds", "Symbol", "Class probabilities.")
.add_argument("rpn_bbox_pred_flat", "Symbol", "Location regression predictions.")
.add_argument("ctpn_anchors", "Symbol", "Ctpn prior anchor boxes")
.add_arguments(CtpnDetectionParam::__FIELDS__());
}  // namespace op
}  // namespace mxnet
